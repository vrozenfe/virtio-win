#~/bin/sh

umount mnt_iso
umount mnt_iso_old
rm freeze_results.txt
rm compare_results.txt
rm *.rpm
rm -rf usr

rmdir mnt_iso
rmdir mnt_iso_old

rm -rf virtio-win-prewhql-*
